function htmlNewTabButton () {
  return `
    <script src="${chrome.extension.getURL('less.js')}"></script>
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" onload="console.log('LOADED', Date.now()) || window.postMessage('GSTYLE_LOADED', '*')" rel="stylesheet">
    <style type="text/less">
    #curate_button_id {
        cursor:pointer;
        margin-left: -125px !important;
        text-decoration: none !important;
        color: #fff !important;
        text-align: center !important;
        position: fixed !important;
        z-index: 2147483647 !important;
        right: 5% !important;
        bottom: 30px !important;
        font-size: 17px !important;
        width: 62px !important;
        font-family: poppins !important;
        height: 62px !important;
        box-shadow: 10px -10px rgba(0,0,0,0.6) !important;
        -moz-box-shadow: 10px -10px rgba(0,0,0,0.6) !important;
        -webkit-box-shadow: 5px 5px 10px #D3D3D3 !important;
        -o-box-shadow: 10px -10px rgba(0,0,0,0.6) !important;
        border-radius: 100px !important;
        display: none;
    }
    .radial-progress {
        @circle-size: 62px;
        @circle-background: grey;
        @circle-color: black;
        @inset-size: 45px;
        @inset-color: grey;
        @transition-length: 1s;
        @shadow: 6px 6px 10px rgba(0,0,0,0.6);
        @percentage-color: #97a71d;
        @percentage-font-size: 22px;
        @percentage-text-width: 57px;

        margin: 50px;
        width:  @circle-size;
        height: @circle-size;

        background-color: @circle-background;
        border-radius: 50%;
        .circle {
            .mask, .fill, .shadow {
                width:    @circle-size;
                height:   @circle-size;
                position: absolute;
                border-radius: 50%;
            }
            .shadow {
                box-shadow: @shadow inset;
            }
            .mask, .fill {
                -webkit-backface-visibility: hidden;
                transition: -webkit-transform @transition-length;
                transition: -ms-transform @transition-length;
                transition: transform @transition-length;
                border-radius: 50%;
            }
            .mask {
                clip: rect(0px, @circle-size, @circle-size, @circle-size/2);
                .fill {
                    clip: rect(0px, @circle-size/2, @circle-size, 0px);
                    background-color: @circle-color;
                }
            }
        }
        .inset {
            width:       @inset-size;
            height:      @inset-size;
            position:    absolute;
            margin-left: (@circle-size - @inset-size)/2;
            margin-top:  (@circle-size - @inset-size)/2;

            background-color: @inset-color;
            border-radius: 50%;
            box-shadow: @shadow;
            .percentage {
                height:   @percentage-font-size;
                width:    @percentage-text-width;
                overflow: hidden;

                position: absolute;
                top:      (@inset-size - @percentage-font-size) / 2;
                left:     (@inset-size - @percentage-text-width) / 2;

                line-height: 1;
                .numbers {
                    margin-top: -@percentage-font-size;
                    transition: width @transition-length;
                    span {
                        width:          @percentage-text-width;
                        display:        inline-block;
                        vertical-align: top;
                        text-align:     center;
                        font-weight:    800;
                        font-size:      @percentage-font-size;
                        font-family:    "Poppins", "Lato", "Helvetica Neue", Helvetica, Arial, sans-serif;
                        color:          @percentage-color;
                    }
                }
            }
        }

        @i: 0;
        @increment: 180deg / 100;
        .loop (@i) when (@i <= 100) {
            &[data-progress="@{i}"] {
                .circle {
                    .mask.full, .fill {
                        -webkit-transform: rotate(@increment * @i);
                        -ms-transform: rotate(@increment * @i);
                        transform: rotate(@increment * @i);
                    }
                    .fill.fix {
                        -webkit-transform: rotate(@increment * @i * 2);
                        -ms-transform: rotate(@increment * @i * 2);
                        transform: rotate(@increment * @i * 2);
                    }
                }
                .inset .percentage .numbers {
                    width: @i * @percentage-text-width + @percentage-text-width;
                }
            }
            .loop(@i + 1);
        }
        .loop(@i);
    }
    #curate_button_id .__badge {
        position: absolute;
        top: -8px;
        right: -5px;
        padding: 4px 4px;
        border-radius: 50%;
        background: #f50000;
		width: 12px;
        color: white;
        font-size: 11px;
		font-weight: 700;
        display: none;
		height: 16px;
		width: 16px;
		font-family: poppins, arial;
    }
    #extensionNotifications {
        width: 250px;
        position: fixed;
        right: 2%;
        bottom: 150px;
        font-family: poppins, arial;
        z-index: 9999;
    }
    </style>
    <div id="extensionNotifications"></div>
    <div id="curate_button_id" class="radial-progress" data-progress="0">
        <div class="circle">
            <div class="mask full">
                <div class="fill"></div>
            </div>
            <div class="mask half">
                <div class="fill"></div>
                <div class="fill fix"></div>
            </div>
            <div class="shadow"></div>
            <span style="
                font-weight: 400 !important;
                font-size: 44px;
                position: relative;
                height: 28px !important;
                line-height: 20px !important;
                top: 21px;
                font-family:poppins !important;
            ">C</span>
        </div>
        <span class="__badge" ></span>
    </div>`
}
window['htmlNewTabButton'] = htmlNewTabButton
