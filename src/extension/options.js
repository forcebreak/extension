// saves options to chrome.storage
function saveOptions () {
  chrome.storage.local.set({
    showforall: document.getElementById('showforall').checked,
    showontwitter: document.getElementById('showontwitter').checked,
    dontdisplay: document.getElementById('dontdisplay').checked,
    autorun: document.getElementById('autorun').checked,
    official: document.getElementById('official').checked
  })
}

function restoreOptions () {
  chrome.storage.local.get({showforall: true, showontwitter: true, dontdisplay: false, autorun: false, official: false}, function (items) {
    document.getElementById('showforall').checked = items.showforall
    document.getElementById('showontwitter').checked = items.showontwitter
    document.getElementById('dontdisplay').checked = items.dontdisplay
    document.getElementById('autorun').checked = items.autorun
    document.getElementById('official').checked = items.official
  })
}

document.addEventListener('DOMContentLoaded', restoreOptions)
document.getElementById('showforall').addEventListener('change', saveOptions)
document.getElementById('showontwitter').addEventListener('change', saveOptions)
document.getElementById('dontdisplay').addEventListener('change', saveOptions)
document.getElementById('autorun').addEventListener('change', saveOptions)
document.getElementById('official').addEventListener('change', saveOptions)
