var EXT_URL = `chrome-extension://${chrome.runtime.id}`
var domainlist = []
var loadTimer = null
var dontdisplay = false
var shouldBeOpen = false
var Progress = {
  current: 0,
  step: 5,
  stepTime: 2000,
  interval: null
}
var TimeLoading = {
  SCRIPT: Date.now(),
  DOM: 0,
  STYLE: 0,
  LOAD: 0
}

// var popupErrorHtml = '<div class="alert alert-danger alert-dismissible fade show" role="alert">\
//                         <strong>Holy guacamole!</strong> You should check in on some of those fields below.\
//                         <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
//                           <span aria-hidden="true">&times;</span>\
//                         </button>\
//                       </div>';

var popupErrorHtml = `<style>
                        .alert {
                          padding: 10px;
                          background-color: #f8d7da;
                          border-color: #f5c6cb;
                          color: #721c24;
                          border-radius: 5px;
                          font-size: 12px;
                          line-height: 14px;
                        }
                      </style>  
                      <div id="errorsAlert-extension" class="alert">
                      </div>`

function showError(err) {
  errorsAlert = document.getElementById('errorsAlert-extension');
  if (!errorsAlert) {
    rootEl = document.getElementById('extensionNotifications');
    rootEl.innerHTML = popupErrorHtml;
    errorsAlert = document.getElementById('errorsAlert-extension');
  }
  if (errorsAlert && !errorsAlert.querySelector("div[id='" + err.code + "']")) {
    let errorMesage = document.createElement('div');
    errorMesage.className = "error-message";
    errorMesage.id = err.code
    errorMesage.innerHTML = err.message;
    errorsAlert.appendChild(errorMesage);
  }

}

function process_errors(errors) {
  codes_for_show = [1003, 4004, 4003, 4005]
  for (let i in errors) {
    if (codes_for_show.includes(errors[i].code)) {
      showError(errors[i])
    }
  }
}

function countCharachters (domainName) {
  const domainNameArray = domainName.split('').filter(item => item === '/')
  var count = domainNameArray.length
  return count
}

function loadingFinised () {
  clearInterval(Progress.interval)
  console.log('\tcurrentPercentage STPOP')
  Progress.interval = null
  window['$']('#curate_button_id').css('background-color', 'black')
}

function load () {
  // Got response in content script
  chrome.runtime.onMessage.addListener((request) => {
    if (request.message === 'API') {
      const { data = {} } = request
      if (data.errors && data.errors.length > 0) {
        process_errors(data.errors)
      }
      if (data && data.completion_percentage) {
        const currentPercentage = data.completion_percentage | 0
        if (Progress.current > currentPercentage) {
          // Progress.stepTime = Math.max(6000, (Progress.stepTime * 1.5) | 0)
          Progress.step = Math.max(1, Progress.step - 1)
          initClockAnimation()
        } else {
          Progress.current = currentPercentage
        }
        console.log('data-progress', currentPercentage)
        window['$']('#curate_button_id').attr('data-progress', currentPercentage)
        if (currentPercentage) {
          window['$']('#curate_button_id').attr('data-progress-real', currentPercentage)
        }

        if (shouldBeOpen && (data.stats || {total_tweets: 0}).total_tweets > 0) {
          openWidget_popup()
        }

        if ((data.stats || {total_tweets: 0}).total_tweets > 0) {
          window['$']('#curate_button_id .__badge').text(data.stats.total_tweets);
          window['$']('#curate_button_id .__badge').show();
        }

        if (currentPercentage === 100) {
          loadingFinised()
        }
      }
    }
    return true
  })

  window.addEventListener('message', (message) => {
    if (message.data === 'GSTYLE_LOADED') {
      TimeLoading.STYLE = Date.now()
      setTimeout(_ => {
        window['$']('#curate_button_id').show()
      }, 250)
    }
  }, false)

  window.addEventListener('load', () => {
    TimeLoading.LOAD = Date.now()

    console.log('-------------------------------------------')
    console.log('DOM CONTENT LOADED', TimeLoading.DOM)
    console.log('STYLE LOADED', TimeLoading.STYLE)
    console.log('FULL LOADED', TimeLoading.LOAD)
    console.log('FULL LOADED  - DOM Content Loaded (sec)', (TimeLoading.LOAD - TimeLoading.DOM) / 1000)
    console.log('FULL LOADED  - STYLE LOADED', (TimeLoading.LOAD - TimeLoading.STYLE) / 1000)
    console.log('STYLE LOADED - DOM Content Loaded (sec)', (TimeLoading.STYLE - TimeLoading.DOM) / 1000)
    console.log('DOM Content Loaded - Content script inited (sec)', (TimeLoading.DOM - TimeLoading.SCRIPT) / 1000)
    console.log('-------------------------------------------')
  }, false)

  // Get information about tab id
  chrome.runtime.sendMessage({message: 'TAB_ID'}, (response) => {
    const {tabId} = response
    chrome.storage.onChanged.addListener((changes) => {
      if ('close' in changes && changes.close.newValue && changes.close.newValue.tabId === tabId) {
        const iframe = window['$']('#overlay_Div1 iframe')
        window['$'](iframe).animate({ 'right': '-=600px' }, 'fast', () => {
          window['$']('#overlay_Div1 iframe').remove()
        })
      } else if ('dontdisplay' in changes && changes.dontdisplay) {
        showHideCuration(changes.dontdisplay.newValue)
      }
    })
  })

  loadTimer = setInterval(initCurationDomain, 1000)
}

load()

function initClockAnimation () {
  // Interval JOB
  clearInterval(Progress.interval)
  Progress.interval = setInterval(() => {
    const nextTic = Progress.current + Progress.step
    console.log('Timer tic', Progress.current, Progress.step)
    Progress.current = Math.min(100, nextTic)
    console.log('\tRESULT', Progress.current, nextTic)
    window['$']('#curate_button_id').attr('data-progress', Progress.current)
    if (Progress.current === 100) {
      loadingFinised()
    }
  }, Progress.stepTime)
}

function buttonAnimnation (url) {
  if (document.getElementById('curate_button_id') != null) {
    document.getElementById('curate_button_id').addEventListener('click', function () {
      document.getElementById('curate_button_id').style.animation = 'move 0.3s'
      // document.getElementById('curate_button_id').style.WebkitAnimationFillMode = " forwards";
      document.getElementById('curate_button_id').style.animationduration = ' 0.3s'
      var styleEl = document.createElement('style')
      styleEl.innerHTML = '@keyframes move{' +
             'from{transform: translateX(0); opacity : 1;-webkit-box-shadow: 2px 2px 5px #D3D3D3;animation-duration: 0.15s;}' +
             'to{transform: translate(4px, 4px);' +
             '-webkit-box-shadow: px px 0px #D3D3D3;animation-duration: 0.3s;}' +
             '}'
      document.head.appendChild(styleEl)
      setTimeout(function () {
        // var newUrl = 'http://theconversation.social/social-discovery/?q=' + url
        // console.log(newUrl)
        // var win = window.open(newUrl, '_blank')
        // win.focus()
        document.getElementById('curate_button_id').style.animation = 'unset'
      }, 2000)
    })
  }
}

function createIframe (rootElement) {
  const iframe = document.createElement('iframe')
  iframe.src = `${EXT_URL + '/widget/index.html'}`
  iframe.setAttribute('allowTransparency', 'true');
  iframe.setAttribute('style', `position: fixed;
        top: 0;
        display: block;
        right: -600px;
        width: 500px;
        height: 100%;
        border: none;
        z-index: 99999999999999;
  `)
  // window['$'](iframe).hide()
  rootElement.appendChild(iframe)
  return iframe
}

function openWidget_popup () {
  let iframe = document.querySelector('#overlay_Div1 iframe')
  if (!iframe) {
    addWidget()
    iframe = document.querySelector('#overlay_Div1 iframe')
    shouldBeOpen = true
  }
  const progress = window['$']('#curate_button_id').attr('data-progress-real') || 0
  if (progress) {
    shouldBeOpen = false
    window['$'](iframe).animate({ 'right': '+=600px' }, 'fast')
    // Illia Start
    document.querySelector('#curate_button_id').style.setProperty("z-index", "2147483646", "important")
    iframe.style.setProperty("z-index", "2147483647", "important")
    // Illia End
  }
}

function addWidget () {
  var g = document.querySelector('#overlay_Div1')
  const iframe = document.querySelector('#overlay_Div1 iframe')
  if (!iframe) {
    createIframe(g)
    initClockAnimation()
  }
}

function addCuration (autorun) {
  chrome.storage.local.get({dontdisplay: false}, function (items) {
    dontdisplay = items.dontdisplay
    if (!dontdisplay) {
      var g = document.createElement('div')
      g.setAttribute('id', 'overlay_Div1')
      document.body.appendChild(g)
      document.getElementById('overlay_Div1').innerHTML += window['htmlNewTabButton'](window.location.href)
      if (autorun) {
        addWidget()
      }
      window['less'].refreshStyles()
      window['$']('#curate_button_id').click(openWidget_popup)
      buttonAnimnation(window.location.href)
    }
  })
}

// function getInstancesFromPath (location, instance) {
//   return location.pathname.split('').filter(item => item === instance).length
// }

// function hasDigit (location) {
//   const firstPath = location.pathname.replace('/', '').split('/')[0]
//   return /^\d+$/.test(firstPath)
// }

function initCurationDomain () {
  chrome.storage.local.get({'domainlist': [], 'autorun': false}, storageObj => {
    if (window.location.href.indexOf('twitter.com') !== -1) {
      return true
    }
    if (window.location.href.indexOf('theconversation.social') > -1) {
      return true
    }

    domainlist = storageObj.domainlist || []
    domainlist = domainlist.map(item => item.trim()).filter(item => item)

    // domainlist = domainlist;
    // localStorage.setItem("localStorageKey", JSON.stringify(domainlist));
    let inited = false
    for (let domainlistURL of domainlist) {
      if (window.location.href.indexOf(domainlistURL) > -1) {
        inited = true
        if (window.location.href.indexOf('index.html') > -1) {
          domLoaded(storageObj.autorun)
        } else if (countCharachters(window.location.href) >= 4) {
          // for nbcnews.com, cbsnews.com, msnbc.com, for foxnews.com
          domLoaded(storageObj.autorun)
        } else if (window.location.href.indexOf('.html') > -1) {
          // for nytimes.com, washingtonpost.com, latimes.com
          domLoaded(storageObj.autorun)
        } else if (window.location.href.indexOf('/story/') > -1 || window.location.href.indexOf('/article/') > -1) {
          // for usatoday.com
          domLoaded(storageObj.autorun)
        } else if (window.location.href.indexOf('/2018/') > -1 || window.location.href.indexOf('/2019/') > -1 || window.location.href.indexOf('/2020/') > -1 || window.location.href.indexOf('/2021/') > -1 || window.location.href.indexOf('/2022/') > -1 || window.location.href.indexOf('/2023/') > -1) {
          domLoaded(storageObj.autorun)
        }
      }
    }
    //"comment back in if you want C icon to always appear on non domain pages without autocuration even if it doesn't pass our domainslist rules"
    /* if (!inited) {
      domLoaded(false)
    } */
  })
}

// function hasArticleTags () {
//   let hasTag = document.querySelector('meta[content="article"][property="og:type"]') ||
//               document.querySelector('meta[itemprop="headline"]') ||
//               document.querySelector('link[rel="amphtml"]') ||
//               document.querySelector('[itemtype="http://schema.org/NewsArticle"]')

//   return hasTag || [...document.querySelectorAll('[type="application/ld+json"]')].find(elem => {
//     return elem && ((elem.innerHTML || '').indexOf('NewsArticle') >= 0)
//   })
// }

function showHideCuration (newValue) {
  const overlay = document.querySelector('#overlay_Div1')
  if (!newValue) {
    if (overlay) {
      window['$'](overlay).show()
    } else {
      // initCurationDomain()
    }
  } else {
    window['$'](overlay).hide()
  }
}

function domLoaded (autorun) {
  clearInterval(loadTimer)
  const loadedDom = function () {
    if (document.body) {
      TimeLoading.DOM = Date.now()
      addCuration(autorun)
    } else {
      setTimeout(loadedDom, 100)
    }
  }
  loadedDom()
}
