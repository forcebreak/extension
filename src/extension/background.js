const SERVER = 'theconversation.social'
const EXPIRE_PERIOD = 3600 * 1000
const FREE_TRIAL_COUNT = 200
const EXT_URL = `chrome-extension://${chrome.runtime.id}`

let twitterIds = {}
let loggedInUser = ''
let $ = window['$']
let tabForClose = -1;
let onInstalledTab;

// Set up
window['Module'] = (function () {
  chrome.webNavigation.onBeforeNavigate.addListener(function (info) {
    console.log(info)
    if (info.url && info.frameId === 0) {
      fireBackendInit({data: info.url}, 'onBeforeNavigate').then(_ => {})
    }
  })

  chrome.runtime.onInstalled.addListener(function (){
    chrome.tabs.create({ url: `chrome-extension://${chrome.runtime.id}/pages/popup.html#/carousel` }, function(tab){
      onInstalledTab = tab
    });
  })

  chrome.tabs.onUpdated.addListener(function (tabId, info, tab) {
    if (chrome.runtime.lastError) {
      console.warn('Updated lastError', chrome.runtime.lastError)
    }
    if(info.title && info.title.includes('Twitter')) tabForClose = tabId
    const currentUrl = tab.url
    const checkUrl = 'chrome://extensions'
    const check = currentUrl.includes(checkUrl) || currentUrl.includes('chrome-extension://')
    if (!check) {
      info.url && console.log('onUpdated', info.url)
      if(info.url === 'http://theconversation.social/social-discovery/' && tabForClose != -1) {
        refreshLoggedInStatus()
        // window.loggedInUser = 'sdsdds'
        chrome.tabs.remove(tabForClose, function() { })
        chrome.tabs.remove(onInstalledTab.id, function() { })
      }
      if (info.url) {
        fireBackendInit({data: info.url}, 'onUpdated').then(_ => {})
      }
      if (info.status === 'complete') {
        chrome.tabs.executeScript(null, { file: 'payload.js' }, _ => {
          let e = chrome.runtime.lastError
          if (e !== undefined) {
            console.log(tabId, _, e)
          }
        })
      }
    }
  })

  function createLookupDict (data) {
    var lookupDict = {}
    for (var i = 0; i < data.length; i++) {
      lookupDict[data[i].toLowerCase()] = true
    }
    return lookupDict
  }

  function loadTwitterIds () {
    console.log('lookup table expired, request new twitter list...')
    fetch(`http://${SERVER}/recipient/get_twitter_ids/`, {
      method: 'GET',
      credentials: 'include'
    }).then(res => res.json())
      .then((data) => {
        if (Array.isArray(data)) {
          twitterIds = createLookupDict(data)
          chrome.storage.local.set({twitterIds: JSON.stringify(data), updateDate: Date.now()})
        } else {
          setTimeout(loadTwitterIds, 20 * 1000)
        }
      })
      .catch(function () {
        setTimeout(loadTwitterIds, 20 * 1000)
      })
  }

  // for cards

  function loadDomainLists () {
    console.log('lookup table expired, request new domain list...')
    fetch('https://s3.amazonaws.com/writepublic-uploads/domainlist.txt', {
      method: 'GET',
      credentials: 'include'
    }).then(res => res.text())
      .then(data => {
        const arrDomainList = data.split('\n')
        if (data || arrDomainList.length) {
          chrome.storage.local.set({
            domainlist: arrDomainList,
            updateDate_domainLists: Date.now()
          })
          console.log('local storage updated.')
        } else {
          setTimeout(loadDomainLists, 20 * 1000)
        }
      }).catch(error => {
        console.log('request error. retry 20 seconds later...', error)
        setTimeout(loadDomainLists, 20 * 1000)
      })
  }

  function refreshLoggedInStatus (sendResponse) {
    fetch(`http://${SERVER}/social-discovery/?d=${Math.random()}`, {
      credentials: 'include'
    })
      .then(r => r.text())
      .then(resp => {
        const loggedInUser = $(resp).find(`.user-profile p`).text().trim()
        if (loggedInUser && loggedInUser !== 'Personalize') {
          window.loggedInUser = loggedInUser
        } else {
          window.loggedInUser = ''
        }
        sendResponse && sendResponse(window.loggedInUser)
      })
  }

  chrome.storage.local.get({twitterIds: null, updateDate: null}, (items) => {
    console.log('got twitter list from storage.')
    if (items.twitterIds) {
      twitterIds = createLookupDict(JSON.parse(items.twitterIds))
      console.log(Object.keys(twitterIds).length)
      console.log('twitter lookup table loaded.')
    }
    if (items.twitterIds === null || items.updateDate === null || items.updateDate + EXPIRE_PERIOD < Date.now()) {
      loadTwitterIds()
    }
  })

  // for cards

  chrome.storage.local.get({domainLists: null, updateDate_domainLists: null}, (items) => {
    console.log('got domain list from storage.')
    if (items.domainLists === null || items.updateDate_domainLists === null || items.updateDate_domainLists + EXPIRE_PERIOD < Date.now()) {
      loadDomainLists()
    }
  })

  chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    switch (request.method) {
      case 'fetchRecipientProfile': {
        const url = `http://${SERVER}/recipient/get_data_by_twitter_id/${request.twitterId}/'`
        fetch(url, {
          method: 'GET',
          credentials: 'include'
        }).then(res => res.json())
          .then(r => {
            if (!r.short_desc || !r.image || !r.followers || !r.home_page) {
              // if no data return from server, try to fetch user info from twitter.com
              // console.log(Object.keys(r).length);
              if (Object.keys(r).length === 3) {
                if (r.category === '' && r.subcategory === '') {
                  sendResponse({data: r, success: false})
                } else {
                  r.scrape = 'true'
                  sendResponse({data: r, success: true})
                }
              } else {
                sendResponse({data: r, success: true})
              }
            } else {
              r.scrape = 'false'
              sendResponse({data: r, success: true})
            }
          }).catch(err => {
            console.error(err)
            sendResponse({data: {}, success: false})
          })
        return true
      }

      case 'socialCurationInit': {
        console.log('SOCIALCURATION')
        console.log(request)
        const url = `http://${SERVER}/social_curation/?init=true&url=${request.linkTitle}`
        fetch(url, {
          method: 'GET',
          credentials: 'include'
        }).then(res => res.json())
          .then(r => {
            sendResponse({data: r.link_id, success: true})
          }).catch(err => {
            console.error(err)
            sendResponse({data: {}, success: false})
          })
        return true
      }
      case 'createCollection': {
        console.log(request)
        const url = `http://${SERVER}/social_curation/?createcollection=true&link_id=${request.linkId}`
          fetch(url, {
            method: 'GET',
            credentials: 'include'
          }).then(res => res.json())
            .then(r => {
              console.log('R!!!')
              console.log(r)
              console.log(r.completion_percentage)
              if(r.errors && r.errors[0]) {
                sendResponse({data: {errors:r.errors[0].message}, success: false});
                console.log('sendResponse with error: ', r.errors)
                return 
              }
              if(r.completion_percentage < 100){
                console.log('CREATE COLLECTION!!!')
                console.log(r)
                sendResponse({data: {percentage:r.completion_percentage, tweets:r.stats.total_tweets, collectionURL:r.collection_url}, success: false})
              }
              else if(r.completion_percentage == 100 && r.collection_url == '') sendResponse({data: {percentage:r.completion_percentage, tweets:r.stats.total_tweets, collectionURL:r.collection_url}, success: false})
              else sendResponse({data: {percentage:r.completion_percentage, tweets:r.stats.total_tweets}, success: true})
              
              
              // if(r.collection_url != '') {
              //   sendResponse({data: r, success: true})
              // }
              // else throw error
            })
            .catch(err => {
              console.log('ERROR!!!!')
              console.log(err)
              sendResponse({data: {errors:err}, success: false})
            })
        // return true
      }
      
      
      case 'checkVaildRecipient': {
        console.log('CHECK VALID RECIPIENT')
        console.log(request)
        sendResponse({result: twitterIds.hasOwnProperty(request.twitterId.toLowerCase())})
        break
      }
      case 'checkVaildDomain': {
        chrome.storage.local.get({domainlist: null}, (items) => {
          const checkArr = (typeof (items.domainlist) === 'string' ? JSON.parse(items.domainlist) : items.domainlist) || []
          for (var i = 0; i < checkArr.length; i++) {
            if (checkArr[i].indexOf(request.checkdomain) !== -1) {
              console.log('true')
              sendResponse({result: true})
            } else {
              sendResponse({result: false})
              // return false;
            }
          }
        })
        return true
      }
      case 'checkShowForAllWebsite': {
        chrome.storage.local.get({showforall: true, domainlist: null}, (items) => {
          const checkArr = (typeof (items.domainlist) === 'string' ? JSON.parse(items.domainlist) : items.domainlist) || []
          sendResponse({result: items.showforall, domainlist: checkArr})
        })
        return true
      }
      case 'checkShowOnTwitter': {
        chrome.storage.local.get({showontwitter: true}, (items) => {
          sendResponse({result: items.showontwitter})
        })
        return true
      }
      case 'checkLoggedIn': {
        sendResponse({result: !!loggedInUser})
        break
      }
      case 'refreshLoggedInStatus': {
        refreshLoggedInStatus(sendResponse)
        return true
      }
      case 'login': {
        chrome.tabs.create({url: EXT_URL + '/popup.htm'})
        break
      }
    }
  })

  refreshLoggedInStatus()

  function fireBackendInit (request, reason) {
    return new Promise((resolve) => {
      chrome.storage.local.get({'autorun': false}, storageObj => {
        if (storageObj.autorun || reason === 'manual') {
          const url = request.data
          const urlHash = window['md5'](url)
          const localUrl = localStorage.getItem(urlHash)
          console.log('INIT', request, reason, Date.now())
          if (!localUrl) {
            console.log('\tINIT', request)
            let data = JSON.stringify({time: Date.now()})
            localStorage.setItem(urlHash, data)
            const apiurl = `http://theconversation.social/social_curation/?init=true&url=${url}`
            fetch(apiurl, {credentials: 'include'})
              .then(res => res.json())
              .then(res => {
                console.log('res', res)
                data = JSON.stringify({time: Date.now(), link_id: res.link_id})
                localStorage.setItem(urlHash, data)
                resolve(res)
              })
              .catch(() => {})
          }
        }
      })
    })
  }

  function buildHref (url, dataUrl) {
    if (/theconversation.social/.test(url.href)) {
      url.searchParams.delete('url')
      url.searchParams.append('link_id', dataUrl.link_id)
      return url.href
    } else {
      return `http://theconversation.social/social_curation/?link_id=${dataUrl.link_id}&newscomments=true`
    }
  }

  function backendAPIProxy (request, sender, sendResponse) {
    let {url} = request.data
    url = new URL(request.data.url)
    if (/theconversation.social/.test(url.href) && url.searchParams.has('link_id')) {
      fetch(url.href, {
        method: request.data.method.toUpperCase(),
        credentials: 'include'
      })
        .then(res => res.json())
        .then(res => {
          sendResponse(res)
          chrome.tabs.sendMessage(sender && sender.tab.id, {message: 'API', data: res})
        })
    } else {
      const getUrl = /theconversation.social/.test(url.href) && url.searchParams.has('url') ? url.searchParams.get('url') : url
      const urlHash = window['md5'](getUrl)
      let localUrl = localStorage.getItem(urlHash)
      if (localUrl) {
        localUrl = JSON.parse(localUrl)
        const dataUrl = localUrl
        const href = buildHref(url, dataUrl)
        console.log('HREF', href, url, request.data)
        fetch(href, {
          method: request.data.method.toUpperCase(),
          credentials: 'include'
        })
          .then(res => res.json())
          .then(res => {
            sendResponse(res)
            chrome.tabs.sendMessage(sender && sender.tab.id, {message: 'API', data: res})
          })
      } else {
        fireBackendInit({data: getUrl}, 'manual').then(result => {
          console.log('result', result)
          localStorage.setItem(urlHash, JSON.stringify({time: Date.now(), link_id: result.link_id}))
          sendResponse(result)
        })
      }
    }
  }

  chrome.runtime.onInstalled.addListener(function (info) {
    if (info.reason === 'install') {
      chrome.storage.local.set({'freeTrialCounter': FREE_TRIAL_COUNT})
    }
  })
  chrome.runtime.onMessage.addListener(
    (request, sender, sendResponse) => {
      if (request.message === 'API') {
        backendAPIProxy(request, sender, sendResponse)
      } else if (request.message === 'TAB_ID') {
        sendResponse({tabId: sender && sender.tab.id})
      } else if (request.message === 'linkOpen') {
        fireBackendInit(request, 'a.href click').then(_ => {})
      }
      return true
    })

  function clearOldData () {
    const time = 48 * 60 * 60 * 1000
    const data = Object.entries(localStorage).filter(item => ((Date.now() - item[1].time) > time)).map(item => item[0])
    data.forEach(item => {
      localStorage.removeItem(item)
    })
  }

  // Try to clear each run
  clearOldData()
  // Try to clear each 15 minutes
  setInterval(clearOldData, 15 * 60 * 1000)
  setInterval(refreshLoggedInStatus, 1 * 60 * 1000)
})()
window['SERVER'] = SERVER
