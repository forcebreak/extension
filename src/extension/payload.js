////////////////////////////// Methods for iframe  ////////////////////////////////////////////////////////
function createIframe (rootElement) {
  const iframe = document.createElement('iframe')
  iframe.src = `${EXT_URL + '/widget/index.html'}`
  iframe.setAttribute('allowTransparency', 'true');
  iframe.setAttribute('style', `position: fixed;
        top: 0;
        display: block;
        right: -600px;
        width: 500px;
        border: none;
        height: 100%;
        z-index: 2147483646;
	`)
  // window['$'](iframe).hide()
  rootElement.appendChild(iframe)
  return iframe
}

function openWidget () {
  let iframe = document.querySelector('#overlay_Div1 iframe')
  if (!iframe) {
    addWidget()
    iframe = document.querySelector('#overlay_Div1 iframe')
    shouldBeOpen = true
  }
  // const progress = window['$']('#curate_button_id').attr('data-progress-real') || 0
  // if (progress) {
  //   shouldBeOpen = false
  //   window['$'](iframe).animate({ 'right': '+=600px' }, 'fast')
  // }
}

function addWidget () {
  var ov = document.createElement('div')
  ov.setAttribute('id', 'overlay_Div1')
  document.body.appendChild(ov)
  var styleEl = document.createElement('style')
  styleEl.innerHTML = '@keyframes move{' +
             'from{transform: translateX(0); opacity : 1;-webkit-box-shadow: 2px 2px 5px #D3D3D3;animation-duration: 0.15s;}' +
             'to{transform: translate(4px, 4px);' +
             '-webkit-box-shadow: px px 0px #D3D3D3;animation-duration: 0.3s;}' +
             '}'
  document.head.appendChild(styleEl)
  var g = document.querySelector('#overlay_Div1')
  const iframe = document.querySelector('#overlay_Div1 iframe')
  if (!iframe) {
    createIframe(g)
    // initClockAnimation()
  }
}

// function initClockAnimation () {
//   // Interval JOB
//   clearInterval(Progress.interval)
//   Progress.interval = setInterval(() => {
//     const nextTic = Progress.current + Progress.step
//     console.log('Timer tic', Progress.current, Progress.step)
//     Progress.current = Math.min(100, nextTic)
//     console.log('\tRESULT', Progress.current, nextTic)
//     // window['$']('#curate_button_id').attr('data-progress', Progress.current)
//     if (Progress.current === 100) {
//       loadingFinised()
//     }
//   }, Progress.stepTime)
// }

// function loadingFinised () {
//   clearInterval(Progress.interval)
//   console.log('\tcurrentPercentage STPOP')
//   Progress.interval = null
// }

////////////////////////////////////////////////////////////////////////////////////

window['Payload'] = (function () {
  console.log('Document ready.')
  console.log(window['SERVER'])


  //////////////////////////////////////////////////// Methods for notification ////////////////////////////////////////////////////
  
  function setHandlerForLinks(){
    console.log('SET HANDLERS FOR LINKS')
    console.log($(".request-elem"))
    let allLinks = $(".request-elem")
    for(let i=0; i < allLinks.length; i++){
      if(allLinks[i].childNodes[5].innerText.includes('0 tweets') || allLinks[i].childNodes[5].innerText.includes('ERROR!')) {}
      else {
        allLinks[i].childNodes[1].addEventListener('click',function(e){
          openWidget()
          chrome.storage.local.set({'lastLinkId': e.target.id}, function() {
            console.log('lastLinkId is set to ', e.target.id );
          });
          let position = $('#overlay_Div1 iframe').css('right')
          if(position == '-600px') $('#overlay_Div1 iframe').animate({ 'right': '+=600px' }, 'fast')
        })  
      }
    }
    // $(".request-link").click(function(e){
    //   openWidget()
    //   chrome.storage.local.set({'lastLinkId': e.target.id}, function() {
    //     console.log('lastLinkId is set to ', e.target.id );
    //   });
    //   let position = $('#overlay_Div1 iframe').css('right')
    //   if(position == '-600px') $('#overlay_Div1 iframe').animate({ 'right': '+=600px' }, 'fast')
    // })  
  }

  function updatePercentage(htmlId,response,id){
    for(let i in list){
      if(list[i].id == htmlId) {
        list[i].linkId = id
        if(response.data.errors) list[i].errors = response.data.errors
        else {
          list[i].percentage = parseInt(response.data.percentage)
          list[i].tweets = response.data.tweets
        }
        // <span class='request-status'>${list[i].errors ? `ERROR!` : `[${list[i].tweets} tweets gathered |${list[i].percentage}%]`}  </span>
        $(`#${htmlId}-elem`).replaceWith(`
          <li id="${htmlId}-elem" class="request-elem"> 
            <span id="${list[i].linkId}" class="request-link">${list[i].text}</span> 
            <span class='request-remove'>X</span>
            <span class='request-status'>${list[i].errors ? `ERROR!` : `${list[i].tweets} tweets gathered | ${list[i].percentage}%`}  </span>
          </li>`
        )
      }
    }
  }

  function setHandlerRemove(){
    $(".request-remove")
      .click(function(e){
        let text;
        if(e.target.parentElement.innerText.includes('ERROR')) text = e.target.parentElement.innerText.replace('X','').trim().split('[')[0].replace('\n','').split('\n')[0]
        else text = e.target.parentElement.innerText.replace('X','').trim().split('[')[0].replace('\n','')
        
        for(i in list){
          if(list[i].text === text.replace('\n','')){
            list.splice(i, 1);

          }
        }
        if(list.length == 1){
          $(".request-elem").fadeTo('fast',0.2)
          $("#notification").
          animate({height:HEIGHT_NOTIFICATION},500, () => {
            $(".request-elem").remove()
            $('#list-notification').hide()
            $('.expand').hide()
          });
        }
        else renderList()
      }); 
  }

  function newHeightRender(newHeight,duration){
    $("#notification").animate({height:newHeight},duration, ()=>{
      $(".request-elem").remove()
      // <span class='request-status'>${list[i].errors ? `ERROR!` : `[${list[i].tweets} tweets/${list[i].percentage}%]`}  </span>
      for(let i in list){
        $("#list-notification").append(`
        <li id="${list[i].id}-elem" class="request-elem"> 
          <span id="${list[i].linkId}" class="request-link">${list[i].text}</span>
          <span class='request-remove'>X</span>
          <span class='request-status'>${list[i].errors ? `ERROR!` : `${list[i].tweets} tweets gathered | ${list[i].percentage}%`}  </span>
        </li>`)
      }
      $('#list-notification').show()
      
      setHandlerForLinks()
      setHandlerRemove()
    });
  }

  function renderList(){
    let needHeight = HEIGHT_NOTIFICATION + list.length * HEIGHT_EACH_REQUEST
    $(".request-elem").remove()
    newHeightRender(needHeight,500)
  }
  
  function sleep (time) {
    return new Promise((resolve) => {
      setTimeout(resolve, time)
    })
  }

  function updateLogo(htmlId,str){
    $(`#${htmlId}`).replaceWith(`<div data-twitter-id="lillyspickup" id="${htmlId}" style="display: flex; float:right;" class="related-button" has-listener="true">
                                          <img src="${EXT_URL}/c-icon.png" style="width: 15px;height: 15px;margin-top: 4px;">
                                          <dov style="color: grey;margin-top: 4px;text-transform: none;margin-left: 3px;font-family:system-ui;font-weight:100;">
                                            ${str}
                                          </dov>
                                        </div>`)
  }

  
  function createCollection(htmlId,id) {
    chrome.runtime.sendMessage({
      method: 'createCollection',
      linkId: id
    }, async (response) => {
      console.log('RESPONSE !!!')
      console.log(response)
      if(response.data.errors){
        countRequest = 0
        updatePercentage(htmlId,response,id)
        updateLogo(htmlId,'Failed')
        requests.queue.pop()
        requests.failed.push(1)
      }
      else{
        if(!response.success && countRequest < 10) {
          countRequest++
          // updatePercentage(htmlId,response,id)
          if(response.data.percentage == 100) {
            requests.queue.pop()
            requests.failed.push(1)
            updatePercentage(htmlId,{data: {errors:'empty tweets'}},id)
            updateLogo(htmlId,'Failed')

            countRequest = 0
          }
          else{
            updatePercentage(htmlId,response,id)
            if(countRequest == 9 && response.data.collectionURL != "") countRequest = 0
            await sleep(5000)
            createCollection(htmlId,id)
          }
        }
        else if(!response.success && countRequest >= 10) {
          for(let i in list){
            if(list[i].id == htmlId) list[i].percentage = 0
          }
          countRequest = 0
          requests.queue.pop()
          requests.failed.push(1)
          updateLogo(htmlId,'Failed')
        }
        else{
          countRequest = 0
          // Update percantage        
          updatePercentage(htmlId,response,id)
          
          if(response.data.percentage < 100){
            await sleep(5000)
            createCollection(htmlId,id) 
          }
          else{
            requests.queue.pop()
            requests.success.push(1)
            updateLogo(htmlId,'Success')
          }
          setHandlerForLinks()
        }
      }
      
      if(document.getElementById('notification').style.display == 'none'){
       updateLogo(htmlId,'Canceled')
        return
      }
      
      // $("#text-notification").replaceWith(`<span id='text-notification'>${requests.queue.length} requests in process</span>`);
      if(requests.queue.length == 0) $("#text-notification").replaceWith(`<span id='text-notification'>Related conversation ready to view</span>`);
      else  $("#text-notification").replaceWith(`<span id='text-notification'>Still looking</span>`);
       
      
      // if(!document.getElementById('result-notification')){
      //   $("#notification").fadeIn("slow")
      //   $('#container-noification').append(`<span id='result-notification'>success: ${requests.success.length},  failed: ${requests.failed.length}</span>`);
      // }
      // else{
      //   $("#result-notification").replaceWith(`<span id='result-notification'>success: ${requests.success.length},  failed: ${requests.failed.length}</span>`);
      // }

// Удалить
      // if(requests.success.length > 1) $('.expand').show()

        // if(!hasHandler){
        //   hasHandler = true
        //   $(".expand").click((e)=>{
        //     let height = $("#notification").height()
        //     if(height != HEIGHT_NOTIFICATION) {
        //       $(".request-elem").fadeTo('fast',0.2)
        //       $("#notification").animate({height:HEIGHT_NOTIFICATION},500, () => {
        //         $(".request-elem").remove()
        //         $('#list-notification').hide()
        //       }); 
        //     }
        //     else renderList()
        //   })
        // }
    
    })
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////// Create notification ////////////////////////////////////////////////////
  const HEIGHT_NOTIFICATION = 25
  const HEIGHT_EACH_REQUEST = 20
  
  var requests = {
    queue:[],
    success:[],
    failed:[]
  }
  var hasHandler = false
  var list = []
  var countRequest = 0

  const notifContainer = document.createElement('div')
  const secondContainer = document.createElement('div')
  const notifClose = document.createElement('span')
  const listRequests = document.createElement('ul')
  const notifExpand = document.createElement('span')

  notifClose.className = 'close-notif'
  notifClose.innerText = 'x'
  notifExpand.className = 'expand'
  notifExpand.style = 'display: none'
  notifContainer.id = 'notification'
  notifContainer.className = 'ext_conversation_twitter'
  secondContainer.id = 'container-noification'
  listRequests.id = 'list-notification'
  notifContainer.style = 'display: none'
  listRequests.style = `display:none;
                        margin: 0;
                        list-style: none;
                        padding: 0;`
  secondContainer.style = `bottom: 0px;
                          position: absolute;
                          width: 97%;`
  notifContainer.appendChild(secondContainer)
  notifContainer.appendChild(listRequests)
  secondContainer.appendChild(notifClose)
  secondContainer.appendChild(notifExpand)
  document.body.appendChild(notifContainer)

  $(".close-notif").click(function(){
    $("#notification").animate({height:HEIGHT_NOTIFICATION},100, () => {
      $(".expand").off('click')
      $('.expand').hide()
      hasHandler = false
      list = []
      $("#text-notification").remove()
      $(".request-elem").remove()
      $("#result-notification").remove()
      $("#list-notification").hide()
      $("#notification").hide()
    })
    
    for(field in requests){
      requests[field] = []
    }
  });

  $("#notification").click(function(){
    if(list.length === 1 && !list[0].errors){
      chrome.storage.local.set({'lastLinkId': list[0].linkId}, function() {
        openWidget()
        let position = $('#overlay_Div1 iframe').css('right')
        if(position == '-600px') $('#overlay_Div1 iframe').animate({ 'right': '+=600px' }, 'fast')
      });
    }
  });
  $( "#notification" ).mouseover(function() {
    if($("#notification").height() == HEIGHT_NOTIFICATION && list.length > 1){
      renderList()
    }
  });
  $( "#notification" ).mouseleave(function() {
    let height = $("#notification").height()
    if(height != HEIGHT_NOTIFICATION) {
      $(".request-elem").fadeTo('fast',0.2)
      $("#notification").animate({height:HEIGHT_NOTIFICATION},500, () => {
        $(".request-elem").remove()
        $('#list-notification').hide()
      })
    }
  });
// BUGS IN NEW DESIGN!!!
  // document.getElementsByTagName('body')[0].addEventListener("keydown", event => {
  //   console.log($('.css-1dbjc4n section')[0].childNodes[1].firstChild.firstChild)
  //   $('.css-1dbjc4n section')[0].childNodes[1].firstChild.firstChild.style.cssText = 'padding-top:0px !important; padding-bottom: 400px !important;'
  //   console.log($('.css-1dbjc4n section')[0].childNodes[1].firstChild.firstChild)
  // });
  
/////////////////////////////////////////////////////////////////////////////////////////////

  if (window.location.hostname === window['SERVER']) {
    return
  }
  if (document.location.href.indexOf(window['SERVER'] + '/recipient/') !== -1) {
    return
  }

  if (window.location.hostname === 'twitter.com') {
      
    ////////////////////////////  Styles for iframe  ////////////////////////////////////////////////////
      // var g = document.createElement('div')
      // g.setAttribute('id', 'overlay_Div1')
      // document.body.appendChild(g)
      // var styleEl = document.createElement('style')
      // styleEl.innerHTML = '@keyframes move{' +
      //        'from{transform: translateX(0); opacity : 1;-webkit-box-shadow: 2px 2px 5px #D3D3D3;animation-duration: 0.15s;}' +
      //        'to{transform: translate(4px, 4px);' +
      //        '-webkit-box-shadow: px px 0px #D3D3D3;animation-duration: 0.3s;}' +
      //        '}'
      // document.head.appendChild(styleEl)
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    chrome.runtime.connect().onDisconnect.addListener(function () {
      var needUpdateCharlotteIcons
      if (chrome.runtime.lastError) {
        needUpdateCharlotteIcons = true
        window['$'](document).bind('DOMSubtreeModified', function (e) {


          const ourButtons = $('.related-button')//e.target.querySelectorAll('.related-button') || ''
          
          ////////////////////////////  NEW DESIGN Styles for buttons on each tweet  ////////////////////////////////////////////////////
          if($('.r-1mdbhws').length > 0){
            let divs = $('.r-1mdbhws')
            let shareButtons = $('.css-1dbjc4n.r-1mlwlqe.r-18u37iz.r-18kxxzh.r-1h0z5md')
            for(let i=0; i<divs.length;i++){
              if(!divs[i].hasAttribute('expanded')){
                divs[i].setAttribute('expanded',true)
                divs[i].style.cssText = 'max-width:525px;'
              }
              if(!shareButtons[i].hasAttribute('expanded')){
                shareButtons[i].setAttribute('expanded',true)
                shareButtons[i].style.cssText = 'width:60px'
              }
            }
          }
          ////////////////////////////////////////////////////////////////////////////////////////////////////////
          
          if (ourButtons.length) {
            for (var i = 0; i < ourButtons.length; i++) {
              if(!ourButtons[i].getAttribute('has-listener')){
                ourButtons[i].setAttribute('has-listener',true)

                ourButtons[i].addEventListener("click", function (e){
                  let permalink;
                  let text;
                  if(document.querySelector('.css-1dbjc4n.r-18u37iz.r-1wtj0ep.r-zl2h9q.charlotte-modified') != null){
                    e.preventDefault()
                    permalink = `${e.path[5].childNodes[1].querySelectorAll('a')[1]}`
                    text = e.path[5].childNodes[1].querySelectorAll('.css-901oao.r-hkyrab.r-1qd0xha.r-a023e6.r-16dba41.r-ad9z0x.r-bcqeeo.r-bnwqim.r-qvutc0')[0].innerText
                  }
                  else{
                    permalink = `https://twitter.com${e.path[6].dataset.permalinkPath}`
                    text = e.path[5].innerText.split('More')[1]
                  }
                  if(text.includes('Retweete')){
                    text = text.split('\n')[4]
                  }
                  text = text.substring(0,20) + '...'
                  text = text.trim().replace('\n','')
                  requests.queue.push(1)
                  let kek = $(this).context.id
                  
                  list.push({
                    id:kek,
                    text:text,
                    tweets:0,
                    percentage: 0,
                    linkId: 0
                  })

                  let height = $("#notification").height()
                  let needHeight = HEIGHT_NOTIFICATION + list.length * HEIGHT_EACH_REQUEST
                  
                  if(height != HEIGHT_NOTIFICATION){
                    newHeightRender(needHeight,100)
                  } 
                  
                  if(!document.getElementById('text-notification')) {
                    $("#notification").fadeIn("slow")
                    // $("#container-noification").append(`<span id='text-notification'>${requests.queue.length} requests in process</span>`);
                    $("#container-noification").append(`<span id='text-notification'>Starting to gather related conversation</span>`);
                  }
                  else{
                    // $("#text-notification").replaceWith(`<span id='text-notification'>${requests.queue.length} requests in process</span>`);
                    if(requests.queue.length == 0) $("#text-notification").replaceWith(`<span id='text-notification'>Related conversation ready to view/span>`);
                    else $("#text-notification").replaceWith(`<span id='text-notification'>Still looking</span>`); 
                  }
                  
                  let elem = $(this).context
                  $(this).replaceWith( `<div id="${elem.id}">
                                          <div id="loader" ">
                                            <div class="swirl-big" style="float:right; background-image: url(${EXT_URL}/spinner_200.svg)"></div>
                                          </div>
                                        </div>` );

                  chrome.runtime.sendMessage({
                    method: 'socialCurationInit',
                    linkTitle: permalink 
                  }, (response) => {
                      list[list.length-1].linkId = response.data
                      console.log('\n\nRESPONSE WITH LINK ID:',response.data)
                      createCollection(elem.id,response.data) // <------ REQUEST
                    }
                  )
                
                });
              }
            }
            // console.log(ourButtons)
        }


          needUpdateCharlotteIcons = true
        })
        chrome.runtime.sendMessage({
          method: 'checkShowOnTwitter'
        }, function (response) {
          if (response.result) {
            if (needUpdateCharlotteIcons) {
              needUpdateCharlotteIcons = false
              window['putTooltipOnTwitterPost']()
            }
            window['putRelatedConversation']()
          }
        })
      } else {
        window['$'](document).unbind('DOMSubtreeModified')
      }
      needUpdateCharlotteIcons = true

      window['$'](document).bind('DOMSubtreeModified', function (e) {
        needUpdateCharlotteIcons = false
        // Vitalii Update:
        // Added function call
        window['putRelatedConversation']()
        window['putTooltipOnTwitterPost']()
      })
    })
  } else {
    setInterval(function () {
      chrome.runtime.connect().onDisconnect.addListener(function () {
        var needUpdateCharlotteIcons
        if (chrome.runtime.lastError) {
          needUpdateCharlotteIcons = true
          window['$'](document).bind('DOMSubtreeModified', function () {
            needUpdateCharlotteIcons = true
          })
          chrome.runtime.sendMessage({
            method: 'checkShowForAllWebsite'
          }, function (response) {
            if (response && response.result) {
              var checkArr = response.domainlist
              var checkDo = window.location.hostname
              for (var i = 0; i < checkArr.length; i++) {
                if (checkDo.indexOf(checkArr[i].split('.')[0]) !== -1) {
                  if (needUpdateCharlotteIcons) {
                    needUpdateCharlotteIcons = false
                    window['NewputTooltipOnWebsitePost']()
                  }
                }
              }
            }
          })
        } else {
          window['$'](document).unbind('DOMSubtreeModified')
        }
        window['$'](document).bind('DOMSubtreeModified', function () {
          needUpdateCharlotteIcons = false
        })
      })
    }, 1000)
    // ..
    // setInterval(function() {
    //     chrome.runtime.sendMessage({method: "checkShowForAllWebsite"}, function(response) {
    //         console.log(response.result);
    //         var needUpdateCharlotteIcons = true;
    //         if (response.result) {
    //             if (needUpdateCharlotteIcons) {
    //                 needUpdateCharlotteIcons = false;

    //                 console.log('Update tooltip on Webpage.');
    //                 NewputTooltipOnWebsitePost();
    //             }
    //         }
    //     });
    // }, 5000);

    // setInterval(function() {
    //     chrome.runtime.sendMessage({method: "checkShowForAllWebsite"}, function(response) {
    //         console.log(response.result);
    //         if (response.result) {
    //             NewputTooltipOnWebsitePost();
    //         }
    //     });
    // }, 1000);
  }
})()
