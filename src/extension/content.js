let SERVER = 'api.theconversation.social'
var EXT_URL = `chrome-extension://${chrome.runtime.id}`
const $ = window['$']
let myRoots = []

// chrome.tabs.onMessage.addListener(function (request, sender, sendResponse) {
//   console.log(request)
//   // switch (request.method) {
//   //   case 'extensionInstalled': {
//   //     $('body').prepend('<h1>HELLO WORLD</h1>')
//   //     debugger
//   //     console.log('KEKEKEK')
//   //     // alert('KEK')
//   //   }
//   //   break
//   // }
// })

function htmlTooltipOnTwitter (twitterId, oldNewDesign = '') {
  console.log(twitterId)
  const bioHtml = `
      <div style="display: none; padding: 5px; font-size: 95%; font-family: system-ui, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Ubuntu, Helvetica Neue, sans-serif !important;">
        <div style="text-align: center; margin-bottom: 5px;">
          <strong>SOCIAL INTELLIGENCE BRIEF</strong>
          <img  class="qtip-close" style="float: right; cursor: pointer;width:16px !important; height:16pxpx !important;" width="16px" height="16px"
                src="${EXT_URL + '/close.png'}">
          </img>
        </div>
        <div class="image"></div>
        <div class="bio"></div>
        <div class="category"></div>
        <div class="tags"></div>
        <div class="followers"></div>
        <div class="home-page"></div>
        <div class="related-companies"></div>
        <div class="related-people"></div>
        <div class="nationalities"></div>
        <div class="date-of-birth"></div>
          <span class="csv-data"></span>
      </div>`
  const infoIconHtml = `
      <span class="hasTooltip" data-twitter-id="${twitterId}">
        <img style="${oldNewDesign};position: absolute; margin-bottom: 2px;padding-left: 2px;" src="${EXT_URL + '/information-2-16.png'}"/>
      </span>`
  const result = `
      <div style="float: right;color: #fff;font-size: 11px;text-transform: uppercase;margin-right: -4px;font-weight: 600;padding-left: 0px;padding-right: 0px;">
        ${infoIconHtml + bioHtml}
      </div>`
  return result
}

function relatedConversation (twitterId, oldNewDesign = '', statusId) {
  const infoIconHtml = `
      <div data-twitter-id="${twitterId}" id="${statusId}" style="display: flex;" class="related-button">
        <img src="${EXT_URL + '/c-icon.png'}" style="width: 15px;height: 15px;margin-top: 4px;">
        <dov style="color: grey;margin-top: 4px;text-transform: none;margin-left: 3px; font-family:system-ui;font-weight:100;">
          Related Conversation
        </dov>
      </div>`
  const result = `
      <div style="width:140px; float: right;color: #fff;font-size: 11px;text-transform: uppercase;margin-right: -4px;font-weight: 600;padding-left: 0px;padding-right: 0px;height: 20px;cursor: pointer;">
        ${infoIconHtml}
      </div>`

      // btnId++;
  // window['$'](result).bind("click", function () {
  //   alert('CLICK !!!')
  // })
  
  return result
}

function htmlTooltipOnTwitterCards (twitterId, oldNewDesign = '') {
  console.log(twitterId)
  const bioHtml = `
      <div style="display: none; padding: 5px; font-size: 95%; font-family: system-ui, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Ubuntu, Helvetica Neue, sans-serif !important;">
        <div style="text-align: center; margin-bottom: 5px;">
          <strong>SOCIAL INTELLIGENCE BRIEF</strong>
          <img  class="qtip-close" style="float: right; cursor: pointer;width:16px !important; height:16pxpx !important;" width="16px" height="16px"
                src="${EXT_URL + '/close.png'}">
          </img>
        </div>
        <div class="image"></div>
        <div class="bio"></div>
        <div class="category"></div>
        <div class="tags"></div>
        <div class="followers"></div>
        <div class="home-page"></div>
        <div class="related-companies"></div>
        <div class="related-people"></div>
        <div class="nationalities"></div>
        <div class="date-of-birth"></div>
        <span class="csv-data"></span>
      </div>`
  const infoIconHtml = `
      <a href="${twitterId}" target="_blank">
        <img style="${oldNewDesign};position: absolute; margin-bottom: 2px;padding-left: 2px;" src="${EXT_URL + '/icon-16.png'}"/>
      </a>`
  const result = `
      <div style="float: right;color: #fff;font-size: 11px;text-transform: uppercase;margin-right: -4px;font-weight: 600;padding-left: 0px;padding-right: 0px;">
        ${infoIconHtml + bioHtml}
      </div>`
  return result
}

function htmlTooltipBesideTwitterID (twitterId) {
  const bioHtml = `
      <div style="display: none; padding: 5px; font-size: 95%; font-family: Helvetica Neue,Helvetica,Arial,sans-serif !important;">
        <div style="text-align: center; margin-bottom: 5px;">
          <strong>SOCIAL INTELLIGENCE BRIEF</strong>
          <img class="qtip-close" style="float: right; cursor: pointer;" width="16px" height="16px" src="${EXT_URL + '/close.png'}"></img>
        </div>
        <div class="image"></div>
        <div class="bio"></div>
        <div class="category"></div>
        <div class="tags"></div>
        <div class="followers"></div>
        <div class="home-page"></div>
        <div class="related-companies"></div>
        <div class="related-people"></div>
        <div class="nationalities"></div>
        <div class="date-of-birth"></div>
        <span class="csv-data"></span>
      </div>`
  const infoIconHtml = `
    <img style="margin-bottom: -2px;padding-left: 2px;" class="hasTooltip" data-twitter-id="${twitterId}" src="${EXT_URL + '/information-2-12.png'}"/>
  `
  const result = infoIconHtml + bioHtml
  return result
}

function showTooltip (el, relativeEle) {
  if ($(el).hasClass('qtip-added')) {
    return
  }
  console.log(el)
  el = $(el)
  console.log(el)
  var div = el.next('div')
  var twitterId = el.data('twitter-id') ? el.data('twitter-id') : 'undefined'
  var tooltipDataStatus = 'init'

  var updateTooltip = function (api) {
    chrome.storage.local.get({'freeTrialCounter': 0}, function (item) {
      chrome.runtime.sendMessage({ method: 'checkLoggedIn' }, function (status) {
        if (status.result === true || item.freeTrialCounter > 0) {
          if (tooltipDataStatus === 'loaded') {
            div.find('.loading-message').hide()
            api.set('content.text', div)
            if (item.freeTrialCounter > 0) {
              chrome.storage.local.set({'freeTrialCounter': item.freeTrialCounter - 1})
            }
          } else if (tooltipDataStatus === 'failed') {
            div.find('.loading-message').hide()
            api.set('content.text', div.html() + "<br>We have most users' bios in our database, but this one we seem to have left out. Please contact us so it may be added. We apologize and sorry for any inconvenience.")
          }
        } else {
          div.find('.loading-message').hide()
          var $loginDiv = $('<div>Please <a href="#">Login</a> to view the tooltip.</div>')
          $loginDiv.find('a').click(function () {
            chrome.runtime.sendMessage({ method: 'login' })
          })
          api.set('content.text', $loginDiv)
        }
      })
    })
  }

  el.addClass('qtip-added')
  var displayData = function (r) {
    if (r.image) {
      div.children('.image').html('<img width="100" onError="this.onerror=null;this.src=information-2-12.png;" style="float: left; width: auto;margin-right: 13px;" src="' + r.image + '"/>')
    }
    if (r.related_companies) {
      div.children('.related-companies').html('<span><strong>Related Companies</strong>: ' + r.related_companies.join(', ') + '</span>')
    }
    if (r.related_people) {
      div.children('.related-people').html('<span><strong>Related People</strong>: ' + r.related_people.join(', ') + '</span>')
    }
    if (r.date_of_birth) {
      div.children('.date-of-birth').html('<span><strong>Date of Birth</strong>: ' + r.date_of_birth + '</span>')
    }
    if (r.nationalities) {
      let nationalities = r.nationalities.join(', ')
      if (nationalities) {
        div.children('.nationalities').html('<span><strong>Localities</strong>: ' + nationalities + '</span>')
      }
    }
    if (r.tags) {
      let tags = r.tags.join(', ')
      if (tags) {
        div.children('.tags').html('<span><strong>Tags</strong>: ' + tags + '</span>')
      }
    }
    if (r.short_desc) {
      div.children('.bio').html(r.short_desc)
    }
    if (r.category || r.subcategory) {
      var category = r.category ? r.category : ''
      var subcategory = r.subcategory ? r.subcategory : ''
      div.children('.category').html('<span><strong>Category</strong>: ' + capitalizeFirstLetter(category) + ((category && subcategory) ? ' > ' : '') + capitalizeFirstLetter(subcategory) + '</span>')
    }
    if (r.home_page) {
      div.children('.home-page').html('<span><strong>Homepage</strong>: <a href="' + r.home_page + '" target="_blank">' + r.home_page + '</a></span>')
    }
    if (r.followers) {
      div.children('.followers').html('<span><strong>Followers</strong>: ' + r.followers + '</span>')
    }
    if (r.csv_data) {
      const csvHtml = r.csv_data.map(function (data) {
        const fieldsHtml = data.fields.map(function (field) {
          return field.link ? '<a href="' + field.link + '" target="_blank">' + field.data + '</a>'
            : field.data
        }).join(', ')
        return '<div class="imdb-' + data.label.toLowerCase().replace(/ /g, '-') + '"><span><strong>' + data.label + '</strong>: ' + fieldsHtml + '</span></div>'
      }).join('')
      div.children('.csv-data').html(csvHtml)
    }
  }

  $.fn.qtip.zindex = '999999999'

  el.qtip({
    content: {
      text: function (event, api) {
        chrome.runtime.sendMessage({ method: 'fetchRecipientProfile', twitterId: twitterId }, function (response) {
          // console.log("fetchRecipientProfile = ", response);
          if (response.success) {
            displayData(response.data)
            tooltipDataStatus = 'loaded'
          } else {
            tooltipDataStatus = 'failed'
            // tooltipDataStatus = "We have most users' bios in our database, but this one we seem to have left out. Please contact us so it may be added. We apologize and sorry for any inconvenience.";
          }

          updateTooltip(api)

          setTimeout(function () {
            $('.qtip-close').click(function () { el.qtip('toggle', false) })
          }, 100)
        })

        let loadingMsg = '<div class="loading-message">Fetching data. One moment...'
        loadingMsg += '<div style="text-align:center"><img src="' + EXT_URL + '/ajax-loader-transparent.gif" alt="Loading..." style="width: 16px; height:16px" /></div></div>'

        return loadingMsg
      }
    },
    style: {
      classes: 'ui-tooltip-light ui-tooltip-shadow ui-tooltip-padding',
      width: (Math.round(($(window).width() - 721) / 1.75) - 20) + 'px'
    },
    position: {
      my: 'center left',
      at: 'top right',
      viewport: $(window)
    },
    show: {
      effect: function () {
        $(this).slideDown()
      },
      solo: true
    },
    events: {
      show: function (event, api) {
        // fix for wrong tooltip position of element inside shadow dom
        if (relativeEle) {
          var rect = el[0].getBoundingClientRect()
          var tippos = [rect.right + window.scrollX, rect.top + window.scrollY]

          console.log('position.target: ', tippos)
          api.set('position.target', tippos)
        }

        setTimeout(function () {
          console.log('on tooltip show')
          $('.ui-tooltip').css('z-index', '999999999')
          // $('.ui-tooltip-titlebar').css('height', 'auto');
          // $('.ui-tooltip-titlebar').css('height', 'auto');
          $('.qtip-close').click(function () { el.qtip('toggle', false) })
        }, 100)

        updateTooltip(api)
      }
    },
    hide: {
      fixed: true,
      delay: 6000,
      effect: function () {
        $(this).slideUp()
      }
    }

  })
  el.qtip('toggle', true)
}

function putTooltipOnTwitterBySelector (option) {
  var elementSelector = option.elementSelector
  var nameSelector = option.nameSelector
  var fnCreateTooltip = option.fnCreateTooltip
  $(elementSelector).each(function () {
    var el = $(this)
    if (!el.hasClass('charlotte-modified')) {
      el.addClass('charlotte-modified')

      var twitterId = el.find(nameSelector).text().replace('@', '').toLowerCase()
      // console.log(elementSelector)
      // console.log(nameSelector)
      // console.log(twitterId)
      // console.log(el.find(nameSelector).text())
      // el[0].style.background = "red"
      // twitterId = 'barackobama';
      if (twitterId === '') {
        // twitterId = 'barackobama';
        if (document.querySelector('.css-1dbjc4n.r-ku1wi2.r-utggzx.r-m611by .css-1dbjc4n.r-18u37iz.r-1wbh5a2 span') != null) {
          twitterId = document.querySelector('.css-1dbjc4n.r-ku1wi2.r-utggzx.r-m611by .css-1dbjc4n.r-18u37iz.r-1wbh5a2 span').textContent.replace('@', '').toLowerCase()
        } else if (document.querySelector('.css-4rbku5.css-1dbjc4n.r-19yat4t.r-mk0yit.r-wk8lta.r-9qu9m4 .css-1dbjc4n.r-18u37iz.r-thb0q2 .css-1dbjc4n.r-1iusvr4.r-46vdb2.r-1777fci.r-5f2r5o.r-bcqeeo .css-1dbjc4n.r-18u37iz.r-1wbh5a2 .css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0') != null) {
          twitterId = document.querySelector('.css-4rbku5.css-1dbjc4n.r-19yat4t.r-mk0yit.r-wk8lta.r-9qu9m4 .css-1dbjc4n.r-18u37iz.r-thb0q2 .css-1dbjc4n.r-1iusvr4.r-46vdb2.r-1777fci.r-5f2r5o.r-bcqeeo .css-1dbjc4n.r-18u37iz.r-1wbh5a2 .css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0').textContent.replace('@', '').toLowerCase()
        }
      }
      chrome.runtime.sendMessage({method: 'checkVaildRecipient', twitterId: twitterId}, function (response) {
        if (response.result === true) {
          if (fnCreateTooltip) {
            fnCreateTooltip(el, twitterId)
          } else {
            el.after(htmlTooltipBesideTwitterID(twitterId))
          }
        }
      })
    
    }
  })
}

function putTooltipOnTwitterBySelectorNew (option) {
  var elementSelector = option.elementSelector
  var nameSelector = option.nameSelector
  var fnCreateTooltip = option.fnCreateTooltip
  var isNewDesign = document.querySelectorAll('[data-testid="tweet"] [data-testid="reply"]').length > 0
  $(elementSelector).each(function () {
    var el = $(this)
    
    if (!el.attr('charlotte-modified-1')) {
      el.attr('charlotte-modified-1',true)
      var twitterId;
      let statusId
      
      if(isNewDesign){
        statusId = el.find('a')[2].href.split('status')[1].substring(1)
      }
      else statusId = el.context.parentElement.parentElement.dataset.tweetId

                    // if(isNewDesign) twitterId = el.find(nameSelector).text().split('@')[1].split('\n')[0].toLowerCase()
      twitterId = el.find(nameSelector).text().replace('@', '').toLowerCase()
      
      // twitterId = 'barackobama';
      // if (twitterId === '') {
      //   // twitterId = 'barackobama';
      //   if (document.querySelector('.css-1dbjc4n.r-ku1wi2.r-utggzx.r-m611by .css-1dbjc4n.r-18u37iz.r-1wbh5a2 span') != null) {
      //     twitterId = document.querySelector('.css-1dbjc4n.r-ku1wi2.r-utggzx.r-m611by .css-1dbjc4n.r-18u37iz.r-1wbh5a2 span').textContent.replace('@', '').toLowerCase()
      //   } else if (document.querySelector('.css-4rbku5.css-1dbjc4n.r-19yat4t.r-mk0yit.r-wk8lta.r-9qu9m4 .css-1dbjc4n.r-18u37iz.r-thb0q2 .css-1dbjc4n.r-1iusvr4.r-46vdb2.r-1777fci.r-5f2r5o.r-bcqeeo .css-1dbjc4n.r-18u37iz.r-1wbh5a2 .css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0') != null) {
      //     twitterId = document.querySelector('.css-4rbku5.css-1dbjc4n.r-19yat4t.r-mk0yit.r-wk8lta.r-9qu9m4 .css-1dbjc4n.r-18u37iz.r-thb0q2 .css-1dbjc4n.r-1iusvr4.r-46vdb2.r-1777fci.r-5f2r5o.r-bcqeeo .css-1dbjc4n.r-18u37iz.r-1wbh5a2 .css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0').textContent.replace('@', '').toLowerCase()
      //   }
      // }
      
      if(isNewDesign){
        console.log(document.querySelectorAll(`[id="${statusId}"]`).length)
        if(document.querySelectorAll(`[id="${statusId}"]`).length == 0) fnCreateTooltip(el,twitterId, statusId)
      }
      else fnCreateTooltip(el,twitterId, statusId)
    }
  
  })
}

// for cards

function putTooltipOnTwitterBySelectorCards (option) {
  var elementSelector = option.elementSelector
  var nameSelector = option.nameSelector
  var nameSelectorNum = option.nameSelectorNum
  var fnCreateTooltip = option.fnCreateTooltip
  $(elementSelector).each(function () {
    var el = $(this)
    if (!el.hasClass('charlotte-modified')) {
      el.addClass('charlotte-modified')
      var twitterId = document.querySelectorAll(nameSelector)[nameSelectorNum].href
      let checkdomain = null
      if (document.querySelectorAll('.css-901oao.r-hkyrab.r-1qd0xha.r-1blvdjr.r-16dba41.r-ad9z0x.r-bcqeeo.r-19yat4t.r-bnwqim.r-qvutc0 a')[0]) {
        if (document.querySelectorAll('.css-901oao.r-hkyrab.r-1qd0xha.r-1blvdjr.r-16dba41.r-ad9z0x.r-bcqeeo.r-19yat4t.r-bnwqim.r-qvutc0 a')[0].title) {
          checkdomain = document.querySelectorAll('.css-901oao.r-hkyrab.r-1qd0xha.r-1blvdjr.r-16dba41.r-ad9z0x.r-bcqeeo.r-19yat4t.r-bnwqim.r-qvutc0 a')[0].title.split('.')[0].split('https://')[1]
        }
      } else {
        checkdomain = document.querySelectorAll('.css-1dbjc4n.r-117bsoe.r-mvpalk.r-156q2ks .css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0')[4].innerText
      }
      // console.log(nameSelector);
      // console.log(twitterId);
      // console.log(el.find(nameSelector).text());

      if(twitterId != '' || twitterId != null || twitterId != undefined) {
          console.log(el.find('.css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0'));
          fnCreateTooltip(el, twitterId);
      } else {
          el.after(htmlTooltipBesideTwitterID(twitterId));
      }
      twitterId = 'barackobama';
      if(twitterId == "")
      {
          // twitterId = 'barackobama';
         if(document.querySelector('.css-1dbjc4n.r-ku1wi2.r-utggzx.r-m611by .css-1dbjc4n.r-18u37iz.r-1wbh5a2 span') != null){
         twitterId = document.querySelector('.css-1dbjc4n.r-ku1wi2.r-utggzx.r-m611by .css-1dbjc4n.r-18u37iz.r-1wbh5a2 span').textContent.replace('@','').toLowerCase();
          }
          else if(document.querySelector('.css-4rbku5.css-1dbjc4n.r-19yat4t.r-mk0yit.r-wk8lta.r-9qu9m4 .css-1dbjc4n.r-18u37iz.r-thb0q2 .css-1dbjc4n.r-1iusvr4.r-46vdb2.r-1777fci.r-5f2r5o.r-bcqeeo .css-1dbjc4n.r-18u37iz.r-1wbh5a2 .css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0') != null ){
              twitterId = document.querySelector('.css-4rbku5.css-1dbjc4n.r-19yat4t.r-mk0yit.r-wk8lta.r-9qu9m4 .css-1dbjc4n.r-18u37iz.r-thb0q2 .css-1dbjc4n.r-1iusvr4.r-46vdb2.r-1777fci.r-5f2r5o.r-bcqeeo .css-1dbjc4n.r-18u37iz.r-1wbh5a2 .css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0').textContent.replace('@','').toLowerCase();
          }
      }
      chrome.runtime.sendMessage({method: 'checkVaildDomain', checkdomain: checkdomain}, function (response) {
        if (response.result === true) {
          if (fnCreateTooltip) {
            fnCreateTooltip(el, twitterId)
          } else {
            el.after(htmlTooltipBesideTwitterID(twitterId))
          }
        }
      })
    }
  })
}

function putTooltipOnTwitterPost () {
  var newDesignRight = 'right:20px'
  var oldDesignRight = 'right: 30px;padding-top: 3px;'
  var newUerRight = 'right: 20px;     bottom: 20px;'
  var embNew = 'position: inherit !important;bottom: 1px; margin-right: 4px;    padding-left: 6px !important;'
  var oldPerRight = 'padding-top: 2px;'
  // https://twitter.com/
  if (document.querySelector('.stream-item-header') != null) {
    putTooltipOnTwitterBySelector({
      elementSelector: '.stream-item-header',
      nameSelector: '.account-group .username b',
      fnCreateTooltip: function (el, twitterId) {
        el.find('.ProfileTweet-action--more').before(htmlTooltipOnTwitter(twitterId, oldDesignRight))
        // el.parent().find('.ProfileTweet-action.ProfileTweet-action--favorite.js-toggleState').after(relatedConversation(twitterId, oldDesignRight))
      }
    })
  }
   else {
    putTooltipOnTwitterBySelector({
      elementSelector: '.css-1dbjc4n.r-18u37iz.r-1wtj0ep.r-zl2h9q',
      // nameSelector: '.css-76zvg2.css-bfa6kz.r-1re7ezh.r-18u37iz.r-1qd0xha.r-a023e6.r-16dba41.r-ad9z0x.r-bcqeeo.r-qvutc0 span',
      nameSelector: '.css-1dbjc4n.r-18u37iz.r-1wbh5a2.r-1f6r7vd .css-901oao.css-bfa6kz.r-1re7ezh.r-18u37iz.r-1qd0xha.r-a023e6.r-16dba41.r-ad9z0x.r-bcqeeo.r-qvutc0 span',
      fnCreateTooltip: function (el, twitterId) {
        el.find('.css-1dbjc4n.r-18u37iz.r-1h0z5md.r-1joea0r').before(htmlTooltipOnTwitter(twitterId, newDesignRight))
      }
    })
  }
  // https://twitter.com/kylegriffin1
  if (document.querySelector('.ProfileHeaderCard-screennameLink') != null) {
    putTooltipOnTwitterBySelector({
      elementSelector: '.ProfileHeaderCard-screennameLink',
      nameSelector: '.username b'
    })
  } else {
    putTooltipOnTwitterBySelector({
      elementSelector: '.css-1dbjc4n.r-ku1wi2.r-utggzx.r-m611by .css-1dbjc4n.r-18u37iz.r-1wbh5a2 span',
      nameSelector: '.css-1dbjc4n.r-18u37iz.r-1wbh5a2 span'
    })
  }
  // https://twitter.com/i/profiles/popup?user_id=96951817&wants_hovercard=true&_=1503405298280
  if (document.querySelector('.ProfileCard-screennameLink') != null) {
    putTooltipOnTwitterBySelector({
      elementSelector: '.ProfileCard-screennameLink',
      nameSelector: '.username b'
    })
  } else {
    // putTooltipOnTwitterBySelector({
    //   // elementSelector: ".css-1dbjc4n.r-18u37iz.r-1wbh5a2 .css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0",
    //   // nameSelector: '.css-1dbjc4n.r-18u37iz.r-1wbh5a2 .css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0',
    //   // elementSelector: '.css-4rbku5.css-1dbjc4n.r-19yat4t.r-mk0yit.r-wk8lta.r-9qu9m4 .css-1dbjc4n.r-18u37iz.r-thb0q2 .css-1dbjc4n.r-1iusvr4.r-46vdb2.r-1777fci.r-5f2r5o.r-bcqeeo .css-1dbjc4n.r-18u37iz.r-1wbh5a2 .css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0',
    //   elementSelector: '.css-4rbku5.css-1dbjc4n.r-19yat4t.r-mk0yit.r-wk8lta.r-9qu9m4 .css-1dbjc4n.r-18u37iz.r-thb0q2 .css-1dbjc4n.r-1iusvr4.r-46vdb2.r-1777fci.r-5f2r5o.r-bcqeeo',
    //   nameSelector: '.css-4rbku5.css-1dbjc4n.r-19yat4t.r-mk0yit.r-wk8lta.r-9qu9m4 .css-1dbjc4n.r-18u37iz.r-thb0q2 .css-1dbjc4n.r-1iusvr4.r-46vdb2.r-1777fci.r-5f2r5o.r-bcqeeo .css-1dbjc4n.r-18u37iz.r-1wbh5a2 .css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0',
    //   fnCreateTooltip: function (el, twitterId) {
    //     el.find('.css-1dbjc4n.r-18u37iz.r-1h0z5md.r-1joea0r').before(htmlTooltipOnTwitter(twitterId, newUerRight))
    //   }
    // })
  }
  // https://twitter.com/i/moments/898265818974343168
  putTooltipOnTwitterBySelector({
    elementSelector: '.css-1dbjc4n.r-1awozwy.r-6koalj.r-18u37iz.r-1wbh5a2.r-vlx1xi.r-156q2ks .css-1dbjc4n.r-18u37iz.r-1wbh5a2.r-1f6r7vd',
    nameSelector: '.css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0',
    fnCreateTooltip: function (el, twitterId) {
      el.find('.css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0').before(htmlTooltipOnTwitter(twitterId, embNew))
    }
  })
  putTooltipOnTwitterBySelector({
    elementSelector: '.MomentCapsuleItemTweet',
    nameSelector: '.MomentUserByline-username .username b',
    fnCreateTooltip: function (el, twitterId) {
      el.find('.ProfileTweet-action--more').before(htmlTooltipOnTwitter(twitterId, oldPerRight))
      // el.parent().find('.ProfileTweet-action.ProfileTweet-action--favorite.js-toggleState').after(relatedConversation(twitterId, oldDesignRight))
    }
  })

  // type card 1
  if (document.querySelector('.css-901oao.r-hkyrab.r-1qd0xha.r-1blvdjr.r-16dba41.r-ad9z0x.r-bcqeeo.r-19yat4t.r-bnwqim.r-qvutc0 a') != null) {
    document.querySelectorAll('.css-901oao.r-hkyrab.r-1qd0xha.r-1blvdjr.r-16dba41.r-ad9z0x.r-bcqeeo.r-19yat4t.r-bnwqim.r-qvutc0 a').forEach(function (res, n) {
      if (res.href.indexOf('https://t.co') !== -1) {
        putTooltipOnTwitterBySelectorCards({
          elementSelector: '.css-901oao.r-hkyrab.r-1qd0xha.r-1blvdjr.r-16dba41.r-ad9z0x.r-bcqeeo.r-19yat4t.r-bnwqim.r-qvutc0 a',
          nameSelectorNum: n,
          nameSelector: '.css-901oao.r-hkyrab.r-1qd0xha.r-1blvdjr.r-16dba41.r-ad9z0x.r-bcqeeo.r-19yat4t.r-bnwqim.r-qvutc0 a',
          fnCreateTooltip: function (el, twitterId) {
            el.find('.css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0').before(htmlTooltipOnTwitterCards(twitterId, newUerRight))
          }
        })
      }
    })
  }

  // type card 2,3

  if (document.querySelector('.css-1dbjc4n.r-117bsoe.r-mvpalk.r-156q2ks a') != null) {
    document.querySelectorAll('.css-1dbjc4n.r-117bsoe.r-mvpalk.r-156q2ks a').forEach(function (res, n) {
      if (res.href.indexOf('https://t.co') !== -1) {
        putTooltipOnTwitterBySelectorCards({
          elementSelector: '.css-901oao.r-hkyrab.r-1qd0xha.r-1blvdjr.r-16dba41.r-ad9z0x.r-bcqeeo.r-19yat4t.r-bnwqim.r-qvutc0',
          nameSelectorNum: n,
          nameSelector: '.css-1dbjc4n.r-117bsoe.r-mvpalk.r-156q2ks a',
          fnCreateTooltip: function (el, twitterId) {
            el.find('.css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0').before(htmlTooltipOnTwitterCards(twitterId, newUerRight))
          }
        })
      }
    })
  }

  $('.hasTooltip').each(function () {
    if (!$(this).hasClass('mouseover-subscribed')) {
      $(this).addClass('mouseover-subscribed').on('mouseover', function () {
        showTooltip(this)
      })
    }
  })
}

function putRelatedConversation () {
  var newDesignRight = 'right:20px'
  var oldDesignRight = 'right: 30px;padding-top: 3px;'
  // var newUerRight = 'right: 20px;     bottom: 20px;'
  // var embNew = 'position: inherit !important;bottom: 1px; margin-right: 4px;    padding-left: 6px !important;'
  // var oldPerRight = 'padding-top: 2px;'
  // https://twitter.com/
  if (document.querySelector('.stream-item-header') != null) {   // Selectors for OLD twitter design
    putTooltipOnTwitterBySelectorNew({
      elementSelector: '.stream-item-header',
      nameSelector: '.account-group .username b',
      fnCreateTooltip: function (el, twitterId, statusId) {
        el.parent().find('.ProfileTweet-action.ProfileTweet-action--favorite.js-toggleState').after(relatedConversation(twitterId, oldDesignRight, statusId))
      }
    })
  } 
  else if(document.querySelector('[data-testid="tweet"]') != null){  // Selectors for NEW twitter design
    putTooltipOnTwitterBySelectorNew({
      elementSelector: '[data-testid="tweet"]',
      nameSelector: '.css-901oao.css-bfa6kz.r-1re7ezh.r-18u37iz.r-1qd0xha.r-a023e6.r-16dba41.r-ad9z0x.r-bcqeeo.r-qvutc0 span',
      fnCreateTooltip: function (el, twitterId, statusId) {
        var elem = $(relatedConversation(twitterId, newDesignRight, statusId))
        const lastElem = $(el.parent().find('[data-testid="reply"]')[0].parentNode.parentNode).last();
        if(lastElem.height() > 19) {
          elem.css('margin-top',0);
        }
        else elem.appendTo(lastElem)
      }
    })
  }
  // else if(document.querySelector('.css-1dbjc4n.r-18u37iz.r-1wtj0ep.r-zl2h9q.charlotte-modified') != null){  // Selectors for NEW twitter design
  //   console.log('\n\nNEW DESIGN!!!')
  //   putTooltipOnTwitterBySelectorNew({
  //     elementSelector: '.css-1dbjc4n.r-18u37iz.r-1wtj0ep.r-zl2h9q.charlotte-modified',
  //     nameSelector: 'css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0',
  //     fnCreateTooltip: function (el, twitterId, statusId) {
  //       el.parent().find('.css-1dbjc4n.r-1mlwlqe.r-18u37iz.r-18kxxzh.r-1h0z5md').after(relatedConversation(twitterId, newDesignRight, statusId))
  //     }
  //   })
  // }
  else{

  }
  // https://twitter.com/kylegriffin1
  if (document.querySelector('.ProfileHeaderCard-screennameLink') != null) {
  } else {
  }
  // https://twitter.com/i/profiles/popup?user_id=96951817&wants_hovercard=true&_=1503405298280
  if (document.querySelector('.ProfileCard-screennameLink') != null) {
  } else {
  }
  // https://twitter.com/i/moments/898265818974343168
  // putTooltipOnTwitterBySelector({
  //   elementSelector: '.css-1dbjc4n.r-1awozwy.r-6koalj.r-18u37iz.r-1wbh5a2.r-vlx1xi.r-156q2ks .css-1dbjc4n.r-18u37iz.r-1wbh5a2.r-1f6r7vd',
  //   nameSelector: '.css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0',
  //   fnCreateTooltip: function (el, twitterId) {
  //     el.find('.css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0').before(htmlTooltipOnTwitter(twitterId, embNew))
  //   }
  // })
  // putTooltipOnTwitterBySelector({
  //   elementSelector: '.MomentCapsuleItemTweet',
  //   nameSelector: '.MomentUserByline-username .username b',
  //   fnCreateTooltip: function (el, twitterId) {
  //     el.find('.ProfileTweet-action--more').before(htmlTooltipOnTwitter(twitterId, oldPerRight))
  //     el.parent().find('.ProfileTweet-action.ProfileTweet-action--favorite.js-toggleState').after(relatedConversation(twitterId, oldDesignRight))
  //   }
  // })

  // type card 1
  // if (document.querySelector('.css-901oao.r-hkyrab.r-1qd0xha.r-1blvdjr.r-16dba41.r-ad9z0x.r-bcqeeo.r-19yat4t.r-bnwqim.r-qvutc0 a') != null) {
  //   document.querySelectorAll('.css-901oao.r-hkyrab.r-1qd0xha.r-1blvdjr.r-16dba41.r-ad9z0x.r-bcqeeo.r-19yat4t.r-bnwqim.r-qvutc0 a').forEach(function (res, n) {
  //     if (res.href.indexOf('https://t.co') !== -1) {
  //       putTooltipOnTwitterBySelectorCards({
  //         elementSelector: '.css-901oao.r-hkyrab.r-1qd0xha.r-1blvdjr.r-16dba41.r-ad9z0x.r-bcqeeo.r-19yat4t.r-bnwqim.r-qvutc0 a',
  //         nameSelectorNum: n,
  //         nameSelector: '.css-901oao.r-hkyrab.r-1qd0xha.r-1blvdjr.r-16dba41.r-ad9z0x.r-bcqeeo.r-19yat4t.r-bnwqim.r-qvutc0 a',
  //         fnCreateTooltip: function (el, twitterId) {
  //           el.find('.css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0').before(htmlTooltipOnTwitterCards(twitterId, newUerRight))
  //         }
  //       })
  //     }
  //   })
  // }

  // type card 2,3

  // if (document.querySelector('.css-1dbjc4n.r-117bsoe.r-mvpalk.r-156q2ks a') != null) {
  //   document.querySelectorAll('.css-1dbjc4n.r-117bsoe.r-mvpalk.r-156q2ks a').forEach(function (res, n) {
  //     if (res.href.indexOf('https://t.co') !== -1) {
  //       putTooltipOnTwitterBySelectorCards({
  //         elementSelector: '.css-901oao.r-hkyrab.r-1qd0xha.r-1blvdjr.r-16dba41.r-ad9z0x.r-bcqeeo.r-19yat4t.r-bnwqim.r-qvutc0',
  //         nameSelectorNum: n,
  //         nameSelector: '.css-1dbjc4n.r-117bsoe.r-mvpalk.r-156q2ks a',
  //         fnCreateTooltip: function (el, twitterId) {
  //           el.find('.css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0').before(htmlTooltipOnTwitterCards(twitterId, newUerRight))
  //         }
  //       })
  //     }
  //   })
  // }

  // $('.hasTooltip').each(function () {
  //   if (!$(this).hasClass('mouseover-subscribed')) {
  //     $(this).addClass('mouseover-subscribed').on('mouseover', function () {
  //       showTooltip(this)
  //     })
  //   }
  // })
}

function NewputTooltipOnWebsitePost () {
  var oldPerRight = 'padding-top: 2px;'
  // https://twitter.com/

  NewputTooltipOnWebsiteBySelector({
    elementSelector: 'twitter-widget',
    nameSelector: 'twitter-widget',
    fnCreateTooltip: function (el, twitterId) {
      el.after(htmlTooltipOnTwitter(twitterId, oldPerRight))
    }
  })

  $('twitter-widget').each(function (n) {
    const domElement = document.querySelectorAll('twitter-widget')[n].shadowRoot.querySelectorAll('.hasTooltip')[0]
    const $element = $(domElement)
    if (!$element.hasClass('mouseover-subscribed')) {
      $($element)
        .addClass('mouseover-subscribed')
        .on('mouseover', function () {
          showTooltip($element, document.querySelectorAll('twitter-widget')[n].shadowRoot)
        })
    }
  })
}

function NewputTooltipOnWebsiteBySelector (option) {
  var elementSelector = option.elementSelector
  var fnCreateTooltip = option.fnCreateTooltip
  $(elementSelector).each(function (n) {
    var el = $(document.querySelectorAll('twitter-widget')[n].shadowRoot.querySelectorAll('.SandboxRoot.env-bp-350 .TweetAuthor.js-inViewportScribingTarget')[0])
    if (!el.hasClass('charlotte-modified')) {
      el.addClass('charlotte-modified')
      var twitterId = document.querySelectorAll('twitter-widget')[n].shadowRoot.querySelectorAll('.SandboxRoot.env-bp-350 .TweetAuthor-nameScreenNameContainer .TweetAuthor-screenName.Identity-screenName')[0].textContent.replace('@', '').toLowerCase()
      // var twitterId = 'barackobama';
      // if(twitterId == "")
      // {
      //    if(document.querySelector('.css-1dbjc4n.r-ku1wi2.r-utggzx.r-m611by .css-1dbjc4n.r-18u37iz.r-1wbh5a2 span') != null){
      //    twitterId = document.querySelector('.css-1dbjc4n.r-ku1wi2.r-utggzx.r-m611by .css-1dbjc4n.r-18u37iz.r-1wbh5a2 span').textContent.replace('@','').toLowerCase();
      //     }
      //     else if(document.querySelector('.css-4rbku5.css-1dbjc4n.r-19yat4t.r-mk0yit.r-wk8lta.r-9qu9m4 .css-1dbjc4n.r-18u37iz.r-thb0q2 .css-1dbjc4n.r-1iusvr4.r-46vdb2.r-1777fci.r-5f2r5o.r-bcqeeo .css-1dbjc4n.r-18u37iz.r-1wbh5a2 .css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0') != null ){
      //         twitterId = document.querySelector('.css-4rbku5.css-1dbjc4n.r-19yat4t.r-mk0yit.r-wk8lta.r-9qu9m4 .css-1dbjc4n.r-18u37iz.r-thb0q2 .css-1dbjc4n.r-1iusvr4.r-46vdb2.r-1777fci.r-5f2r5o.r-bcqeeo .css-1dbjc4n.r-18u37iz.r-1wbh5a2 .css-901oao.css-16my406.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0').textContent.replace('@','').toLowerCase();
      //     }
      // }
      chrome.runtime.sendMessage({method: 'checkVaildRecipient', twitterId: twitterId}, function (response) {
        if (response.result === true) {
          if (fnCreateTooltip) {
            fnCreateTooltip(el, twitterId)
          } else {
            el.after(htmlTooltipBesideTwitterID(twitterId))
          }
        }
      })
    }
  })
}

function putTooltipBesideTwitterID () {
  function walkThroughTextNodes (node, twId) {
    $(node).find(':not(iframe)')
      .addBack()
      .contents()
      .filter(function () {
        return this.nodeType === Node.TEXT_NODE
      }).each(function () {
        const newNodeValue = this.nodeValue.replace(new RegExp('(' + twId + ')', 'ig'), function (idRef, idName) {
          return idRef + '<charlottetooltip data-twitter-id="' + twId + '" />'
        })
        if (newNodeValue !== this.nodeValue) {
          $(this).replaceWith($('<span>' + newNodeValue + '</span>')[0])
        }
      })
  }

  function walkThroughANodes (node) {
    var nodeName = node.nodeName.toLowerCase()
    if (node.nodeType !== 1 && node.nodeType !== 11) return null
    else if (nodeName === 'style') return null
    else if (nodeName === 'script') return null
    else if (nodeName === 'charlottetooltip') return null
    else if (!node.classList.contains('charlotte-modified')) {
      var href = node.getAttribute('href')
      if (href) {
        var matches = href.match(/^https?:\/\/twitter.com\/(\w+)\/?$/)
        if (matches && matches[1]) {
          node.classList.add('charlotte-modified')
          walkThroughTextNodes(node, matches[1])
        }
      }
    } else {
      for (var i = 0, len = node.childNodes.length; i < len; ++i) {
        walkThroughANodes(node.childNodes[i])
      }
      if (node.shadowRoot) {
        myRoots.push(node)
        walkThroughANodes(node.shadowRoot)
      }
    }
  }

  myRoots = [document.body]
  walkThroughANodes(document.body)
}

function createCharlotteTooltips () {
  myRoots.map(function (root) {
    var nodes = (root.shadowRoot || root).childNodes
    for (const node of nodes) {
      $(node).find('charlottetooltip').each(function () {
        var self = this
        var twitterId = $(self).data('twitter-id')
        chrome.runtime.sendMessage({method: 'checkVaildRecipient', twitterId: twitterId}, function (response) {
          console.log('checkVaildRecipient', response)
          if (response.result === true) {
            $(self).replaceWith(htmlTooltipBesideTwitterID(twitterId))
          } else {
            $(self).remove()
          }
        })
      })
      $(node).find('.hasTooltip').each(function () {
        if (!$(this).hasClass('mouseover-subscribed')) {
          $(this).addClass('mouseover-subscribed').on('mouseover', function () {
            showTooltip(this, (root.shadowRoot && root))
          })
        }
      })
    }
  })
}

function capitalizeFirstLetter (string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

window['putTooltipOnTwitterPost'] = putTooltipOnTwitterPost
window['createCharlotteTooltips'] = createCharlotteTooltips
window['putTooltipBesideTwitterID'] = putTooltipBesideTwitterID
window['NewputTooltipOnWebsitePost'] = NewputTooltipOnWebsitePost
window['putRelatedConversation'] = putRelatedConversation
window['SERVER'] = SERVER
// $('body').on('click', 'related-button', function() {
//   console.log('here')
//   // do something
// });

// var actualCode = `
//   var items = document.querySelectorAll(".related-button");
//   console.log("ITEMS !!!")
//   console.log(items)
//   for (var i = 0; i < items.length; i++) {
//     items[i].addEventListener("click", function (){
//       alert("Hello!"); 
//     });
//   }`
    
// var script = document.createElement('script');
// script.textContent = actualCode;
// (document.head||document.documentElement).appendChild(script);