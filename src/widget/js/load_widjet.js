
window['SERVER_API'] = 'http://theconversation.social/social_curation/'

const options = {
  display_mode: 'full',
  speaker_types: {
    public_figures: true,
    other_public_figures: true,
    media: true,
    the_public: true,
    news_comments: true
  },
  required_speaker_types: {
    public_figures: false,
    other_public_figures: false,
    media: false,
    the_public: false,
    news_comments: false
  },
  news_comments_from_domains: [],
  news_comments_types: {
    disqus_comments: true,
    fb_comments: true,
    fb_plugin_comments: true,
    fyre_comments: true,
    g_comments: true,
    nytimes_comments: true,
    viafoura_comments: true
  },
  max_results_per_section: {
    public_figures: 0,
    other_public_figures: 0,
    media: 0,
    the_public: 0,
    news_comments: 0
  },
  max_results: 10,
  min_popularity: 0,
  category_tags_whitelist: '',
  id: 'ROOT_ELEMENT',
  allow_low_confidence: true,
  allow_news: 'storydevelopmet,widelyreported',
  bio: 'wikipedia', // 'original', 'wikipedia', 'category', false
  show_community_feedback: true,
  category_section_headers: true,
  header: 'Top Social Conversation',
  display_top_section_header: true,
  css_file: 'style.css',
  max_queries_before_stop: 30,
  query_frequency_seconds: 10, // how often to query api in seconds
  placeholder_html: '',
  placeholder_frequency: 0,
  storydevlopment_prefer_date: false
}

var Ajax = {
  request: function (ops, callback) {
    chrome.runtime.sendMessage({message: 'API', data: ops}, (response) => {
      console.log('ÁPI', response)
      callback && callback(response)
    })
  }
}

function loadingInfo (tabInfo, linkId) {
  const apiurl = 'http://theconversation.social/social_curation/'
  let url
  if (linkId) {
    url = `${apiurl}?createcollection=true&link_id=${linkId}`
  } else {
    url = tabInfo.url
  }
  return new Promise((resolve) => {
    Ajax.request({url, method: 'get'}, resolve)
  })
}

function sleep (time) {
  return new Promise((resolve) => {
    setTimeout(resolve, time)
  })
}

function loadJs () {
  var head = document.getElementsByTagName('head')[0]
  var script = document.createElement('script')
  script.src = 'https://platform.twitter.com/widgets.js'
  head.appendChild(script)
}

async function enableOfficialTwitterWidget (tabInfo, lastLinkId=null) {
  let count = 0
  let linkId = lastLinkId
  while (count < 30) {
    const response = await loadingInfo(tabInfo, linkId)
    linkId = response.link_id
    if (response.collection_url) {
      const ROOT_ELEMENT = document.querySelector('#ROOT_ELEMENT')
      if (ROOT_ELEMENT) {
        ROOT_ELEMENT.innerHTML = `
          <a class="twitter-timeline" href=${response.collection_url}>The Conversation</a>
        `
        loadJs()
        break
      }
    }
    await sleep(5000)
    count++
  }
}

function closeHandler (tabInfo) {
  console.log(window['$']('.bottom_bar'))
  window['$']('.bottom_bar').hide()
  window['$']('#close').click(() => {
    chrome.storage.local.set({
      'close': {
        tabId: tabInfo.id,
        rnd: Math.random()
      }
    })
  })
}

function enableOwnWidget (tabInfo) {
  window['Module'] && window['Module'](window)
  window['initWidget'] && window['initWidget'](options, tabInfo.url, Ajax)
  closeHandler(tabInfo)
}

chrome.tabs.getCurrent(tabInfo => {
  // debugger
  chrome.storage.local.get({official: false, lastLinkId: null}, items => {
    const location = new URL(tabInfo.url)
    if (location.hostname === 'twitter.com' && items.lastLinkId) {
      enableOfficialTwitterWidget(tabInfo, items.lastLinkId).then(_ => {
        chrome.storage.local.set({'lastLinkId': null}, function() {
          console.log('lastLinkId is set to null');
        });
        closeHandler(tabInfo)
      })
    } else if (items.official) {
      enableOfficialTwitterWidget(tabInfo).then(_ => {
        closeHandler(tabInfo)
      })
    } else {
      enableOwnWidget(tabInfo)
    }
  })
})

window['$'](document).on('click', 'a', (evt) => {
  const url = evt.target.href || window['$'](evt.target).closest('a').prop('href')
  if (/https?:\/\//.test(url)) {
    evt.preventDefault()
    chrome.tabs.create({url})
    return false
  }
})
